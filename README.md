# Chambéry Contraception Thermique

Ce dépot contient le site statique du collectif chambérien sur la CTT, avec :

* Des liens vers des ressources générales sur la méthode thermique
* Des adresses utiles chambériennes (labo où faire ses spermogrammes, médecins, etc)
* La vie du collectif (date et lieux des ateliers, archive des newsletter, etc)
