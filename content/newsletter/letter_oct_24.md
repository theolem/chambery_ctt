Title: Octobre 2024
Author: Chambéry Contraception Thermique
Date: 10/17/2024
Category: Newsletters
Slug: newsletter-oct-2024

C'est bon, le collectif Coup de chaud est officiellement rentré de vacances !

Le collectif continue à se structurer pour faire connaître les méthodes de contraception testiculaires sur Chambéry.

Au programme de cette newsletter, deux nouvelles dates d'ateliers à faire tourner sur vos réseaux, un article écrit de notre plus belle plume, et un résumé des événements où on a été présent⋅es depuis la rentrée (merci Loïc !)

### Prochains ateliers

**Deux nouvelles dates d'ateliers sont prévues !**

- le **jeudi 31 octobre** 2024 de 19h à 21h
- le **mercredi 27 novembre** 2024 de 19h à 21h

Les ateliers auront lieu à l'**Insolente**, lieu autogéré au **50 Faubourg Montmélian à Chambéry**, qui nous avait déjà accueilli plusieurs fois l'année dernière.

On vous a prévu pour ces ateliers : des cercles de paroles autour de nos parcours contraceptifs, et un brief explicatif sur le fonctionnement de la spermatogénèse et sur les protocoles de contraceptions testiculaires. On espère vous y voir nombreu⋅ses, seul⋅es ou accompagné⋅es, curieu⋅ses ou déjà convaincu⋅es. Les ateliers sont gratuits et en mixité.

En bonus, on vous joint dans cette newsletter un visuel à faire tourner sur vos réseaux sociaux si vous en avez, et que vous voulez faire connaître l'activité du collectif !

![Visuel Instagram annonçant les dates d'atelier, le 31 octobre 2024 et le 27 novembre 2024](../images/oct_2024.png)

Et comme d'habitude, toutes nos informations sont sur [coupdechaud.fr](https://coupdechaud.fr/).

### Activités du collectif

On a publié **un article sur Dahut.info, le site d'information de lutte de Chambéry**, intitulé ["La contraception testiculaire, à Chambéry et ailleurs"](https://dahut.info/article/local/la-contraception-testiculaire-a-chambery-et-ailleurs-4019). On y parle de l'histoire des contraceptions testiculaires, et de pourquoi on inscrit notre collectif dans une lutte pour l'égalité des genres. Suivez Dahut, et retrouvez nos ateliers sur l'agenda du site !

On a aussi participé au [**village associatif de rentrée de l'Insolente**](https://dahut.info/article/event/rentree-de-linsolente-rencontre-avec-les-collectifs-concert-samedi-6507) le 4 octobre, aux côtés de nombreuses autres structures et collectifs actifs dans ce lieu autogéré à Chambéry. C'était un moment très sympa et riche en rencontres, merci à l'Inso' d'avoir organisé ce moment.

On a tenu un **stand d'information à la Pride de Chambéry** le 12 octobre. On a aussi signé aux côtés d'autres collectifs chambériens (Tarlouz, la Lessiveuse, le PF73) un [tract d'appel à un cortège radical, décolonial et antifasciste](https://dahut.info/article/event/pride-cortege-radicale-decolonial-et-antifasciste-1466). Au moment où le fascisme est à nos portes, où d'anciens membres de la Manif pour tous sont comme à la maison au gouvernement, où les attaques contre les minorités de genre et les droits reproductifs s'intensifient dans de nombreux pays dont le nôtre, nous souhaitions apporter notre soutien à celleux qui y font face. Les marches de fiertés sont un moment de célébration de nos identités et sexualités dans toute leurs diversités, mais aussi un rappel des luttes en cours.

À très vite !

Le collectif Coup de chaud
