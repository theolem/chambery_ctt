Title: Janvier 2025
Author: Chambéry Contraception Thermique
Date: 01/12/2025
Category: Newsletters
Slug: newsletter-janv-2025

Le collectif Coup de chaud arrive en magie dans cette année 2025 !

### Prochains ateliers

On vous propose deux ateliers qui sortent un peu de notre ordinaire pour les prochains mois :

* Le **mardi 28 janvier 2025**, on vous propose un **atelier couture de jockstrap**. Les [jockstrap ou remonte-couilles toulousains (RCT)](https://www.contraceptionmasculine.org/jockstrap/) sont des dispositifs de contraception thermique qu'il est possible de fabriquer chez soi, à l'aide seulement d'une machine à coudre et d'une dizaine d'euros de mercerie simple. L'atelier se fera **sur inscription uniquement**, pour ne pas être trop nombreu⋅ses : écrivez un mail à **coupdechaud@proton.me** si vous êtes intéressé⋅e. Une participation aux achats de mercerie (5-10€ / jockstrap) est appréciée <3
* Le **jeudi 20 février 2025**, on projettera le documentaire ["Vasectomie : ça va faire mâle"](https://lesnouveauxjours-prod.com/les-projets/vasectomie-ca-va-faire-male/) de Jean François Marquet (lieu et horaire exactes à préciser, stay tuned). Le documentaire suit plusieurs hommes dans leurs parcours de vasectomie, et décrit avec délicatesse l'évolution de la vie affective d'un couple quand les rapports à la contraception s'équilibrent.

### Actualités en vrac
* L'initiative **Slowcontraception** lance la **saison 2 de ses webinaires scientifiques** dédiés à la contraception masculine. "Cette série d’événements en ligne propose un espace d’échange et de réflexion autour des avancées scientifiques, des expériences vécues et des perspectives sociales et médicales." Pour en savoir plus, rendez-vous sur leur Instagram : **@slowcontraception**. Les replay de leurs webinaires de 2024 sont ici : [Février](https://drive.google.com/file/d/1FSN5UdSYQy9ci0mZPQsyJ4j1kYviwhv0/view?pli=1), [mars](https://drive.google.com/file/d/1JTnMDJTAS0-ragoJ-Lx8C0RyGqHnUON4/view), [avril](https://drive.google.com/file/d/1Z2mtXWlv0hLK96Wx9xHyfN841uj_bYf2/view), [mai](https://drive.google.com/file/d/1no2ORo6XSnGVKYYoZbJbKfNvADEELYwl/view).

* La copaine Laétitia, éducatrice menstruelle, fait un atelier le flux libre instinctif - ou comment apprendre à libérer son sang menstruel de manière consciente et volontaire - le 19 janvier à la ferme de la Goettaz (Bourget-du-Lac). C'est sur inscription, à cette adresse mail : **laetitia.willecomme@gmail.com**.
