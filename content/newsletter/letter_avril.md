Title: Avril 2024
Author: Chambéry Contraception Thermique
Date: 04/12/2024
Category: Newsletters
Slug: newsletter-av-2024

## Ateliers passés et à venir

* Le premier atelier du collectif a eu lieu le 28 mars à l'Insolente. On a projeté le documentaire **"Haut les couilles" de Sidone Hadoux**, on a beaucoup discuté, et on bien content⋅es ! Merci à tout le monde d'avoir été présent⋅es, merci à l'Inso de nous avoir accueilli⋅es, merci au vidéoprojecteur d'avoir fini par marcher, et merci à la réalisatrice de nous avoir permi de projeter ce super docu !
* Le prochain atelier aura lieu le **mardi 23 avril 2024 au Bruit qui Court** à Chambéry, de 19h à 21h. On prévoit un cercle de parole autour de nos parcours contraceptifs, suivi d'une présentation de notre part sur le fonctionnement de la contraception thermique. On espère vous voir nombreu⋅ses !
* Les prochains ateliers sont prévus pour les **29 mai et 26 juin**. Retrouvez tout ce qu'on vous prépare [ici](https://coupdechaud.fr/pages/ateliers.html).

## Vie du collectif

On est très content⋅es d'avoir pu faire notre premier atelier ce mois-ci. On espère pouvoir continuer à informer sur la contraception thermique ici à Chambéry, et ouvrir des espaces de discussions sur notre vécu de la contraception.

### Café-planning du 10 avril

Deux d'entre nous ont aussi pu participer au Café Planning sur la contraception organisé le 10 avril par le Planning Familial. On a pu échanger sur nos parcours contraceptifs (variés), apprendre au passage plein de choses sur nos corps. Merci à toutes les personnes du PF 73 pour l'organisation, n'hésitez pas à suivre leur activité foisonnante sur Instagram !

### Testival les 22-23 juin 2024

Le [collectif 13ticule](https://www.instagram.com/13ticules/) organise une rencontre inter-collectif ainsi qu'un festival ouvert au public le weekend du 22-23 juin à Marseille. Au programme, plein de belles choses : des stands, des ateliers, de la musique… on s'y croise ?

Vous pouvez retrouver d'autres actualités sur l'avancée de la contraception testiculaire sur [la gazette du GTCM](https://listes.infini.fr/ardecom.infini.fr/info/gazette-gtcm).

