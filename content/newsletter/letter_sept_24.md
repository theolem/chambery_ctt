Title: Septembre 2024
Author: Chambéry Contraception Thermique
Date: 09/27/2024
Category: Newsletters
Slug: newsletter-sept-2024

Coup de chaud est de retour après l'été !

## Prochains ateliers

On est en train de mettre en place les ateliers pour cette année. On aimerait continuer à organiser des ateliers d'information régulièrement sur les contraceptions testiculaires, et à faciliter l'adoption de ces méthodes par des personnes qui le souhaiteraient : annuaires de médecins, ateliers de couture de jockstrap… Tout sera indiqué sur notre site __coupdechaud.fr__ !

En attendant qu'on s'organise , **vendredi 04 octobre** on sera présents à la rentrée de l'Insolente, espace autogéré à Chambéry, avec les autres collectifs actifs dans ce lieu. [Plus d'information ici](https://dahut.info/article/event/rentree-de-linsolente-rencontre-avec-les-collectifs-concert-samedi-6507).

Et on est toujours joignables à __coupdechaud [at] proton.me__ si vous avez des questions ou envie de se rencontrer !

## Actus et lecture

* Vous avez été contracepté⋅e grâce à la méthode thermique et vous avez arrêté ? [Un questionnaire](https://forms.office.com/Pages/ResponsePage.aspx?id=_BPDZ2Q3CUykoYHzXtzvU8wIjJyrWbZDrC9P-lv1U9RUQlowNkZUUTE0U0RKME80RjhPV1RQS09ZNy4u) tourne en ce moment pour étudier plus en profondeur la réversibilité de cette méthode, dans le cadre d'une thèse de médecine. On vous invite à y répondre si vous êtes concerné⋅e !
* Clément Le Roux, interne en santé publique et membre du collectif de Tours "Les remonté⋅es", a soutenu son mémoire de M2 avec pour titre **« Histoire des groupes d'hommes et des pratiques de contraceptions testiculaires de 1977 à 1985 »**. Il a notamment été fouiller dans les archives de l'Ardecom (association historique en France de recherche sur les contraceptions testiculaires) pour faire l'histoire des collectifs de contraception testiculaire. Vous pouvez retrouver son mémoire en entier sur [la plateforme Dune](https://dune.univ-angers.fr/documents/dune18778).
* Sur le même sujet, on a lu récemment [un article de 2015](https://www.50-50magazine.fr/2015/06/17/lexperience-dardecom-ou-lenvers-de-la-contraception/) sur l'expérience de l'Ardecom et la signification de leurs actions pour l'émergence d'autres modèles de masculinités. On vous invite à y jeter un œil !

À bientôt !
