Title: Juin 2024
Author: Chambéry Contraception Thermique
Date: 06/17/2024
Category: Newsletters
Slug: newsletter-juin-2024

Bonjour à toustes !
Coup de chaud a loupé sa newsletter du mois de mai, mais pas son atelier. On était à l'AQCV le 30 mai, merci à elleux de nous avoir accueillis, et merci à vous qui êtes venu⋅es !

## Prochain atelier le 26 juin !

Le prochain atelier aura lieu à l'Insolente le mercredi 26 juin prochain, de 19h à 21h. On prendra le temps de discuter de la contraception testiculaire (méthode thermique, vasectomie), et de jouer pour celleux qui le souhaitent au jeu de société [Le cœur des zobs](https://www.eclap.fr/collections/eclap/products/le-coeur-des-zobs-pre-commande), qui a été conçu par le collectif 13ticule à Marseille pour provoquer la discussion sur la contraception, la charge contraceptives, les rapports de genre et le rapport au corps, et tous les sujets dont on veut parler à Coup de chaud.

L'Insolente est un espace autogéré situé au 50 Faubourg Montmélian à Chambéry, on vous invite à venir à leurs événements et à vous informer sur [leur site](https://linsolente.lautre.net/) et sur place si vous ne le connaissez pas.

## Les actus

### Le testival à Marseille c'est ce weekend !

Le collectif 13ticules (encore elleux) organisent ce weekend (22-23 juin) une rencontre entre collectifs qui travaillent sur la contraception testiculaire à Marseille. Avec Coup de chaud, on y sera. Le dimanche sera ouvert au public, si vous êtes dans le coin, n'hésitez pas à passer ! Les infos sont surtout sur l'Instagram de 13ticule et de Bobika.

### Enquête Cobalt

Vous êtes utilisateur⋅ice de la contraception thermique ou vous réfléchissez à vous y mettre ? Cobalt pourrait avoir besoin de vous pour son enquête. Ça se passe [ici](https://docs.google.com/forms/d/e/1FAIpQLScNlsIMHmZ-GcJH3rH3CwdkdH3zM81tN3Eca9JgXINqRDJPpg/viewform).

[Cobalt](https://cobalt-contraception.com/) c'est une structure en lancement qui réfléchit à commercialiser des dispositifs contraceptifs thermiques. Ça pourrait être cool non ?
