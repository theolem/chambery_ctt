Title: Mars 2024
Author: Chambéry Contraception Thermique
Date: 03/12/2024
Category: Newsletters
Slug: newsletter-mar-2024

## On lance un collectif !

Nous nous sommes retrouvé⋅es à quelques personnes faisant l'expérience de la contraception testiculaire thermique. C'est une méthode (détaillée un peu plus bas) qui permet de se contracepter quand on a un pénis.

Aujourd'hui, dans les couples hétéros, la charge contraceptive repose quasiment entièrement sur les femmes. On inculque aux femmes dès leur plus jeune âge que c'est à elles de se débrouiller pour éviter les grossesses non désirées, et la contraception reste largement un impensé pour les hommes cis. Pourtant les conséquences des moyens de contraceptions les plus répendus (pillules hormonales, stérilets…) sur la santé des femmes sont parfois sévères.

Mais ces méthodes de contraception ne sont pas les seules : il en existe d'autres quand on a un pénis, réversibles (méthode thermique, méthode hormonale moins répendue) ou pas (vasectomie).

Elles ne sont pas parfaites pour tout le monde, et elle ne vise en aucun cas à remplacer les méthodes existentes. Mais il existe des méthodes fiables, réversibles et abordables, qui peuvent permettre une gestion plus égalitaire de la charge contraceptive, pour une sexualité plus libre et responsable. Alors, on en parle ?

Le collectif Coup de chaud vise à parler de ces méthodes, par des réunions d'informations, et au travers d'ateliers sur la place que prend la contraception dans nos vies, dans les rapports de genre, dans nos sexualités. Nous parlerons principalement de la contraception thermique dont nous faisons nous-même l'expérience, mais aimerions ouvrir la discussion aux autres méthodes.

Nous communiquerons principalement via cette liste mail, dans un premier temps du moins. Si tu connais des gens que ça pourrait intéresser, [pour s'inscrire c'est ici](https://framagroupes.org/sympa/subscribe/contraception_thermique_chambery).

Si tu as des questions, tu peux aussi directement nous contacter à **coupdechaud@proton.me**.

## La contraception thermique, c'est quoi ?

La contraception testiculaire thermique est un moyen **fiable** et **réversible** de se contracepter quand on a un pénis. Il fonctionne sur un principe très simple : la température normale du corps étant légèrement plus élevée que celle des testicules, elle suffit à interrompre la **spermatogénèse**, c'est-à-dire le processus de production de spermatozoïdes.

Il est donc possible, au moyen d'un dispositif adapté (il en existe de plusieurs sortes), de se contracepter en gardant ses testicules "au chaud". Il suffit de le porter 15h par jour, ce n'est ni douloureux, ni désagréable. On vérifie régulièrement l'efficacité du port du dispositif au moyen de **spermogrammes**.

Pour tout savoir sur la contraception thermique, son fonctionnement biologique et le protocole à suivre, tu peux te rendre [sur ce site](https://contraceptionthermique.noblogs.org/introduction/).

## Les ateliers à venir

Les dates seront précisées bientôt.

**Jeudi 28 Mars à l'Insolente** : Projection du documentaire "Haut les couilles" de Sidonie Hadoux.

**Avril** : Cercle de parole : quelle place prend la contraception dans nos vies ?

**Mai** : Réunion d'information autour de la contraception thermique.

**Juin** : Réunion d'information et partie de jeu de société "Le cœur des zobs", réalisé par le collectif marseillais 13ticule.

**Juillet** (peut-être) : Atelier couture de jockstrap.

Les ateliers sont ouverts à toustes, peu importe le genre ou le niveau de connaissance sur la contraception thermique. Tu peux venir pour toi, pour un⋅e ami⋅e, seul⋅e ou accompagné⋅e… Tu seras lea bienvenu⋅e !

## Les actualités

* Un épisode récent du podcast "Les couilles sur la table" parle de fertilité et de masculinité. [Retrouvez-le à cette adresse](https://www.binge.audio/podcast/les-couilles-sur-la-table/les-bourses-ou-la-vies). On peut aussi réécouter [l'épisode de 2018](https://www.binge.audio/podcast/les-couilles-sur-la-table/contraception-masculine-au-tour-des-hommes) sur la contaception dite masculine.

Vous pouvez retrouver d'autres actualités sur l'avancée de la contraception testiculaire sur [la gazette du GTCM](https://listes.infini.fr/ardecom.infini.fr/info/gazette-gtcm).
