Title: Novembre 2024
Author: Chambéry Contraception Thermique
Date: 11/23/2024
Category: Newsletters
Slug: newsletter-nov-2024

Une courte newsletter de novembre pour annoncer les prochains ateliers du collectif Coup de chaud :

* [**Le mercredi 27 novembre de 19h à 21h à l’Insolente**](https://dahut.info/article/event/atelier-contraceptions-testiculaires-a-linsolente-6466), atelier de présentation et de discussion autour des contraceptions testiculaires. Vous êtes curieu⋅se de comment ça fonctionne ? Vous êtes déjà informé⋅e mais vous avez encore quelques doutes ou quelques questions ? Vous souhaitez discuter sans attente particulière ? Nous serons heureu⋅ses de vous rencontrer ! L’atelier est ouvert à toustes, mixte et à prix libre (pour rembourser quelques frais d’impression).
* Prochainement (décembre ou janvier), nous souhaitons organiser un **atelier de couture de jockstrap**. Le jockstrap est un dispositif de contraception thermique qu’il est possible de réaliser chez soi, à l’aide d’une machine à coudre et de quelques euros de mercerie. Cet atelier nécessitant plus de logistique et d’organisation que nos ateliers de présentation, ça nous aiderait d’avoir une idée de combien de personnes sont intéressées. Si vous aimeriez venir, n’hésitez pas à nous le signaler à notre adresse mail : _coupdechaud@proton.me_ .

À très vite !
