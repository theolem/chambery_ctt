Title: Index
Author: Chambéry Contraception Thermique
Date: 12/04/2023
Category: Pages
Slug: index
Hidden: True

## La contraception testiculaire, qu'est-ce que c'est ?

On parle de contraception testiculaire pour désigner les différentes méthodes de contraception qu'on peut utiliser quand on a un pénis. Tout bonnement.

Il existe plusieurs méthodes :

* La méthode thermique est un moyen **fiable** et **réversible** de se contracepter. Il fonctionne sur un principe très simple : la température normale du corps étant légèrement plus élevée que celle des testicules, elle suffit à interrompre la **spermatogénèse**, c'est-à-dire le processus de production de spermatozoïdes. Il est donc possible, au moyen d'un dispositif adapté (il en existe de plusieurs sortes), de se contracepter en gardant ses testicules "au chaud" à l'intérieur du corps. Il suffit de porter ce dispositif 15h par jour, ce ne doit être ni douloureux, ni désagréable. On vérifie régulièrement l'efficacité du port du dispositif au moyen de **spermogrammes**.

Pour en savoir plus sur la contraception thermique, son fonctionnement biologique et le protocole à suivre, tu peux te rendre [sur ce site](https://contraceptionthermique.noblogs.org/introduction/).

* La méthode hormonale est plus confidentielle, mais existe aussi : souvent au moyen d'un gel de testostérone ou d'une injection, on envoie un message chimique au cerveau pour qu'il stoppe la production de spermatozoïdes. Ce n'est donc pas vraiment une "pillule pour homme", mais c'est aussi une méthode fiable et réversible testée avec succès depuis plusieurs dizaines d'années.

* La vasectomie est un moyen **définitif** de bloquer l'arrivée des spermatozoïdes dans le sperme, en bloquant le canal déférent. Elle n'a d'impact ni sur la libido, ni sur l'ejaculation. Elle est largement répandue dans certains pays (Royaume-Uni, Canada), mais assez peu en France.

L'adoption de ces méthodes doit être discutée au sein du couple le cas échéant, et pour être efficace le protocole être suivi avec sérieux.


## Le collectif

Nous sommes trois personnes qui utilisons la méthode thermique pour nous contracepter. Nous avons décidé de créer ce collectif pour visibiliser les méthodes de contraception testiculaires… mais pas que. Au fur et à mesure des discussions, on espère pouvoir parler de comment les rapports de domination se logent dans notre intimité, et apprendre ensemble comment mieux lutter contre le sexisme.

C'est pourquoi on aimerait aborder collectivement le sujet des contraceptions testiculaires en lien avec les luttes politiques antipatriarcales.

Nous comptons au cours de ce printemps 2024 animer une [série d'atelier](./pages/ateliers.html) autour de la contraception testiculaire sur Chambéry, avec :

* une projection de film
* des ateliers d'informations
* des ateliers "cercles de paroles" autour de notre rapport à la contraception
* un atelier couture de dispositif contraceptif (peut-être).

À terme, on aimerait pouvoir tenir des permanences régulières autour de la contraception testiculaire. On verra.

Le collectif est mixte, et toute personne souhaitant s'y impliquer est la bienvenue.

## Sur Chambéry

**Médecins** : Nous essayons de répertorier le personnel médical formé au suivi de la contraception testiculaire sur Chambéry (pour faire les ordonnances de spermogrammes, interprêter les résultats, faire les palpations…), et aussi des médecins compréhensifs et ouverts au sujet de la vasectomie.

Si tu connais quelqu'un⋅e de bien, tu peux nous en faire part en [nous contactant](./pages/contact.html).

**Spermogrammes** : Tu peux faire tes spermogrammes au [laboratoire Biogroup Tercinet](https://laboratoires.biogroup.fr/savoie/chambery/5-rue-favre), au 5 rue Favre dans le centre ([RDV sur Doctolib](https://www.doctolib.fr/laboratoire/chambery/biogroup-laboschambery-site-de-tercinet-amp)). Il te faut une ordonnance pour la prise en charge sécu. La part mutuelle (à ta charge si tu n'en as pas) est d'environ 14€.

En attendant, si tu as des questions ou besoin de conseils, tu peux nous envoyer un mail ou nous voir à nos ateliers. Tu peux te tenir au courant des actualités sur la contraception testiculaire à Chambéry [en t'inscrivant à notre newsletter](https://framagroupes.org/sympa/subscribe/contraception_thermique_chambery).

## Ailleurs

Tu n'es pas sur Chambéry ? Pas de problème, la famille est grande.

Pas loin, il y a [les Sans-Gamètes sur Grenoble](https://www.le-tamis.info/structure/les-sans-gametes).

Et plus loin, [y'a aussi du monde](https://contraceptionthermique.noblogs.org/les-tuyaux-du-slip/).

## Ressources

Beaucoup de sites rassemblent des informations sur la contraception thermique, son protocole, les précautions à prendre, des témoignages de personnes contraceptées, etc.

Nous on aime bien :

* [contraceptionthermique.noblogs.org](https://contraceptionthermique.noblogs.org/), qui ont édité le livre "S'occuper de son sperme". C'est une source d'info très complète pour commencer. Le livre est disponible gratuitement en version web sur le site.

* [Le site d'Otoko](https://samflam.notion.site/samflam/Otoko-Contraception-Autonome-93fd30b3034d465096fc939959ce01d8), initiative parisienne. Sam propose aussi des ateliers à prix libre sur Paris pour fabriquer son anneau contraceptif en silicone.

* [Le site de Thoreme](https://thoreme.com/), l'entreprise qui commercialise l'androswitch. Tu pourras y trouver des conseils sur le port de l'anneau, mais aussi des informations plus générales sur la contraception testiculaire.

… et y'en a plein d'autres !

**Si vous souhaitez parler à un⋅e médecin** et que vous vous demandez comment vous y prendre, on vous conseille aussi [la page dédiée aux médecins du site Thoreme](https://thoreme.com/la-contraception-masculine/) (section "Pour les médecins"). Vous y trouverez, en langage de médecins, des informations chiffrées et sourcées pour convaincre des practicien⋅nes frileu⋅ses de vous accompagner.
