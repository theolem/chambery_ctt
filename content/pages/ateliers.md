Title: Ateliers
Author: Chambéry Contraception Thermique
Date: 12/04/2023
Category: Pages
Slug: ateliers

## À venir

Nous proposons des ateliers d'échange et d'information autour des contraceptions testiculaires. On aime généralement d'abord inviter les personnes présentes qui le souhaitent à raconter leur parcours contraceptif. Ensuite, on transmet les connaissances qu'on a sur les contraceptions testiculaires : comment fonctionne la spermatogénèse, où et comment interviennent la vasectomie et la contraception thermique, quel protocole il est conseillé de suivre, etc.

Les prochaines dates :

* **Jeudi 13 mars 2025 à Chambéry** : atelier de discussion et d'information sur la contraception thermique et la vasectomie. L'atelier aura lieu à la Maison des Associations (67 Rue Saint-François de Sales), salle 102, de 19 à 21h.  

## Passés
* **Mardi 28 janvier 2025 à Cognin** : atelier de couture de jockstrap.
[**27 novembre 2024 à l'Insolente (Chambéry)**](https://dahut.info/article/event/atelier-contraceptions-testiculaires-a-linsolente-6466).
* [**20 novembre 2024** à 18h30 À l'Insolente (Chambéry)](https://dahut.info/article/event/soiree-jeux-a-linsolente-8947) : soirée jeux de société "Mascu-Féministe".
* [**31 octobre 2024 à l'Insolente (Chambéry)**](https://dahut.info/article/event/atelier-contraceptions-testiculaires-a-linsolente-3124).
* **12 octobre 2024 à la marche des fiertés de Chambéry** : stand d'information. Voir [ici](https://dahut.info/article/event/pride-cortege-radicale-decolonial-et-antifasciste-1466) le tract pour un cortège radical signé par le collectif.
* **04 octobre 2024 à l'Insolente (Chambéry)** : stand d'information pour le village associatif de rentrée.
* **[26 juin 2024 à l'Insolente](https://linsolente.lautre.net/?page=Article&idArt=874) (Chambéry)** : partie de jeu de société "[Le cœur des zobs](https://www.eclap.fr/collections/eclap/products/le-coeur-des-zobs-pre-commande)".
* **Jeudi 30 mai 2024 à l'AQCV (Chambéry)** : cercle de parole autour de la contraception.
* **19 mai 2024 au Chat Noir (Isère)**.
* **Mardi 23 avril 2024 au Bruit qui Court (Chambéry)** : la contraception thermique, c'est quoi ? Cercle de parole sur nos parcours contraceptifs et séance d'information sur la contraception testiculaire thermique.
* **[28 Mars 2024 à l'Insolente](https://linsolente.lautre.net/?page=Article&idArt=851)** : projection du documentaire «&nbsp;[Haut les couilles]((https://www.safirhdf.fr/film.php?filmid=200))&nbsp;» de Sidonie Hadoux.
* **14 octobre 2023 au Flamoute (Chambéry)** : atelier de discussions en petit groupes sur nos contraceptions : fiabilité, acceptabilité, accessibilité.
